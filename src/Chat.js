import lyrics from './utils/Lyric';

const Chat = class {
  constructor(bots) {
    this.el = document.body;
    this.bots = bots;

    this.run();
  }

  renderBot(bot) {
    const { name, avatar } = bot;

    return `
        <li class="list-group-item d-flex justify-content-between align-items-center">
          <img class="rounded-circle " width="50" src="${avatar}" alt="avatar">
          ${name} 
        </li>
        `;
  }

  renderListBots() {
    return `
          <ul class="list-group list-group-flush">
            ${this.bots.map((bot) => this.renderBot(bot)).join('')}
          </ul>
        `;
  }

  render() {
    return `
      <div class="d-flex flex-column vh-100">
      <nav class="navbar bg-danger shadow-lg">
            <div class="container-fluid">
              <span class="navbar-brand mb-0 h1 text-light">NinjaBot-Io</span> 
              <button id="clear-messages-btn" class="btn btn-warning">Effacer les messages</button>
            </div>
          </nav>
          <div class="lyrics-container bg-danger">
          <div class="lyrics">
              ${lyrics}
            </div>
          </div>
          <div class="container-fluid mt-2 flex-grow-1 d-flex flex-column">  
          <div class="row flex-grow-1">
              <div class="col-3">
                ${this.renderListBots()}
              </div>
              <div class="col-9 d-flex flex-column">
              <div class="card chat-box flex-grow-1">
              <div class="card-body">
                          <ul class="chat-messages list-unstyled"></ul>
                      </div>
                      <div class="card-footer chat-input">
                          <div class="input-group">
                              <input type="text" id="message-input" class="form-control" placeholder="Tapez votre message...">
                              <div class="input-group-append ml-2">
                                  <button id="send-btn" class="btn btn-primary">Envoyer</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        `;
  }

  displayStoredMessages() {
    const storedMessages = JSON.parse(localStorage.getItem('messages')) || [];
    const ul = document.querySelector('.chat-messages');
    storedMessages.forEach((storedMessage) => {
      const li = this.createMessageElement(
        storedMessage.content,
        storedMessage.isFromBot,
        storedMessage.botName
      );
      ul.appendChild(li);
    });

    ul.scrollTop = ul.scrollHeight;
  }

  clearMessages() {
    localStorage.removeItem('messages');
    const ul = document.querySelector('.chat-messages');
    ul.innerHTML = '';
  }

  saveMessage(message, isFromBot, botName) {
    const messages = JSON.parse(localStorage.getItem('messages') || '[]');

    const maxMessages = 50;
    if (messages.length >= maxMessages) {
      messages.shift();
    }

    messages.push({
      content: message,
      isFromBot,
      botName,
      timestamp: new Date()
    });
    localStorage.setItem('messages', JSON.stringify(messages));
  }

  loadMessages() {
    const ul = document.querySelector('.chat-messages');
    let messages = JSON.parse(localStorage.getItem('messages') || '[]');

    const maxMessages = 50;
    const startIndex = Math.max(messages.length - maxMessages, 0);
    messages = messages.slice(startIndex);

    const messagesHtml = messages
      .map((message) => {
        const li = this.createMessageElement(
          message.content,
          message.isFromBot,
          message.botName
        );
        return li.outerHTML;
      })
      .join('');

    ul.innerHTML = messagesHtml;
  }

  createMessageElement(messageContent, isFromBot, botName) {
    const li = document.createElement('li');
    li.classList.add('clearfix');

    const now = new Date();
    const timeString = `${now.getHours().toString().padStart(2, '0')}:${now
      .getMinutes()
      .toString()
      .padStart(2, '0')}`;

    let formattedMessage = `[${timeString}] Vous : ${messageContent}`;

    if (isFromBot && botName) {
      formattedMessage = `[${timeString}] ${botName} : ${messageContent}`;
      const bot = this.bots.find((b) => b.name === botName);
      if (bot) {
        li.style.backgroundColor = bot.color;
        if (botName.toLowerCase() !== 'zane') {
          li.style.color = 'white';
        }
        li.style.border = '2px solid black';
      }
    }

    li.textContent = formattedMessage;
    li.classList.add(isFromBot ? 'message-bot' : 'message-user');
    this.saveMessage(messageContent, isFromBot, botName);

    return li;
  }

  async processBotResponses(userMessage) {
    const ul = document.querySelector('.chat-messages');
    const promises = this.bots.map((bot) => bot.respondTo(userMessage));

    const responses = await Promise.all(promises);

    responses.forEach((response, index) => {
      if (response) {
        const botName = this.bots[index].name;
        const li = this.createMessageElement(response, true, botName);
        ul.appendChild(li);
        ul.scrollTop = ul.scrollHeight;
      }
    });
  }

  run() {
    this.el.innerHTML = this.render();
    const input = document.getElementById('message-input');
    const ul = document.querySelector('.chat-messages');
    const clearButton = document.getElementById('clear-messages-btn');
    this.displayStoredMessages();

    clearButton.addEventListener('click', () => {
      this.clearMessages();
    });

    const sendMessage = () => {
      const message = input.value.trim();

      if (message) {
        const li = this.createMessageElement(message, false);
        ul.appendChild(li);
        input.value = '';
        ul.scrollTop = ul.scrollHeight;
        this.processBotResponses(message);
      }
    };

    document.getElementById('send-btn').addEventListener('click', sendMessage);
    input.addEventListener('keydown', (e) => {
      if (e.key === 'Enter') {
        sendMessage();
        e.preventDefault();
      }
    });
    this.loadMessages();
  }
};

export default Chat;
