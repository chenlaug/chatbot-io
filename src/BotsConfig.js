import Bot from './Bot';

function generateHelpCommand(actions) {
  return {
    command: 'help',
    type: 'text',
    description: 'Help',
    response: () => {
      let helpText = 'Voici les commandes que je peux comprendre:\n';
      actions.forEach((action) => {
        helpText += `- ${action.command}: ${action.description}\n`;
      });
      return helpText;
    }
  };
}

const commonAction = {
  command: 'salut',
  type: 'text',
  description: 'Saluer l’utilisatuer',
  response: () => 'Salutation à toi !'
};

const lloydActions = [
  commonAction,
  {
    command: 'nature',
    type: 'text',
    description: 'Découvrez le pouvoir de la Création.',
    response: () => 'La puissance du Premier Maître de Spinjitzu, le pouvoir de la Création.'
  },
  {
    command: 'sagesse',
    type: 'text',
    description: 'Recevez une parole de sagesse.',
    response: () => 'Rappelez-vous, le vrai pouvoir vient de l’intérieur.'
  },
  {
    command: 'info',
    type: 'apiCall',
    description: 'Obtenez des informations sur la France.',
    response: async (country = 'France') => {
      try {
        const response = await fetch(
          `https://restcountries.com/v2/name/${country}?fullText=true`
        );
        if (!response.ok) {
          throw new Error('Problème de récupération des données du pays');
        }
        const countryData = await response.json();
        return `Pays: ${countryData[0].name}, Capital: ${countryData[0].capital}, Population: ${countryData[0].population}`;
      } catch (error) {
        return `Erreur: ${error.message}`;
      }
    }
  }
];

const kaiActions = [
  commonAction,
  {
    command: 'astuce de combat',
    type: 'text',
    description: 'Recevez une astuce pour améliorer vos compétences de combat.',
    response: () => 'N’oubliez pas, la clé pour être un bon combattant est l’équilibre et le contrôle.'
  },
  {
    command: 'courage',
    type: 'text',
    description: 'Recevez une citation inspirante sur le courage.',
    response: () => 'Le courage ne crie pas toujours. Parfois, il est la petite voix qui chuchote "j’essaierai encore demain".'
  },
  {
    command: 'motivation',
    type: 'text',
    description: 'Obtenez des mots de motivation.',
    response: () => 'La peur n’est pas mauvaise. Elle vous montre un chemin que vous devez surmonter.'
  },
  {
    command: 'citation',
    type: 'apiCall',
    description: 'Recevez la citation inspirante du jour.',
    response: () => fetch('https://api.quotable.com/random') // URL d'une API fictive pour la citation du jour
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then((data) => `Citation du jour: ${data.content} — ${data.author}`)
      .catch(
        () => 'Désolé, je ne peux pas récupérer de citation en ce moment.'
      )
  }
];

const zaneActions = [
  commonAction,
  {
    command: 'fait',
    type: 'text',
    description: 'Apprenez un fait intéressant.',
    response: () => 'Saviez-vous? Le zéro absolu est la limite inférieure de l’échelle de température thermodynamique.'
  },
  {
    command: 'calme',
    type: 'text',
    description: 'Recevez une citation sur la tranquillité.',
    response: () => 'La paix vient de l’intérieur. Ne la cherchez pas à l’extérieur.'
  },
  {
    command: 'devinette',
    type: 'text',
    description: 'Tentez de résoudre une devinette.',
    response: () => 'Je ne peux être vu que dans les ténèbres et je disparais dès que la lumière apparaît. Que suis-je?'
  },
  {
    command: 'chien',
    type: 'apiCall',
    description: 'Affiche une image aléatoire d’un chien.',
    response: async () => {
      try {
        const response = await fetch('https://dog.ceo/api/breeds/image/random');
        if (!response.ok) {
          throw new Error('Erreur de récupération de l’image du chien');
        }
        const dogData = await response.json();
        return `Voici un chien pour égayer votre journée: ${dogData.message}`;
      } catch (error) {
        return `Erreur: ${error.message}`;
      }
    }
  }
];

const jayActions = [
  commonAction,
  {
    command: 'blague',
    type: 'text',
    description: 'Entendez une blague drôle.',
    response: () => 'Pourquoi les poissons détestent-ils l’ordinateur? Parce qu’ils ont peur du net.'
  },
  {
    command: 'optimisme',
    type: 'text',
    description: 'Recevez une citation optimiste.',
    response: () => 'Souris au monde et le monde te sourira.'
  },
  {
    command: 'énigme',
    type: 'text',
    description: 'Essayez de résoudre une énigme.',
    response: () => 'Plus vous en prenez, plus vous en laissez derrière. Qu’est-ce?'
  }
];

const coleActions = [
  commonAction,
  {
    command: 'force',
    type: 'text',
    description: 'Découvrez d’où vient la vraie force.',
    response: () => 'La force ne vient pas de la capacité physique. Elle vient d’une volonté indomptable.'
  },
  {
    command: 'confiance',
    type: 'text',
    description: 'Apprenez sur l’importance de la confiance en soi.',
    response: () => 'La confiance en soi est le premier secret du succès.'
  },
  {
    command: 'défi',
    type: 'text',
    description: 'Entendez un mot sur les défis personnels.',
    response: () => 'Le plus grand défi est de croire en soi même.'
  }
];

lloydActions.push(generateHelpCommand(lloydActions));
kaiActions.push(generateHelpCommand(kaiActions));
zaneActions.push(generateHelpCommand(zaneActions));
jayActions.push(generateHelpCommand(jayActions));
coleActions.push(generateHelpCommand(coleActions));

export const lloyd = new Bot(
  'Lloyd',
  'https://preview.redd.it/wp4h5zmqd2x81.png?width=1000&format=png&auto=webp&s=c649697b8ed0df5a7dab75036768b7b4029ce22c',
  lloydActions,
  '#008000'
);
export const kai = new Bot(
  'Kai',
  'https://preview.redd.it/k45wyymqd2x81.png?width=1000&format=png&auto=webp&s=588db27f561ed3d90f58fed1dcebb504955766c4',
  kaiActions,
  '#FF0000'
);
export const zane = new Bot(
  'Zane',
  'https://preview.redd.it/q705gzmqd2x81.png?width=1000&format=png&auto=webp&s=3e4808aa869ebb1dd78344cc32fa6fed41f1bb7f',
  zaneActions,
  '#FFFFFF '
);
export const jay = new Bot(
  'Jay',
  'https://preview.redd.it/kr9xgzmqd2x81.png?width=1000&format=png&auto=webp&s=018dc76aea81acea29e1326f499c13ccb883897f',
  jayActions,
  '#0000FF'
);
export const cole = new Bot(
  'Cole',
  'https://preview.redd.it/lpdwn1nqd2x81.png?width=1000&format=png&auto=webp&s=53359886147552c950f2ccd2dba315e6e748a0d5',
  coleActions,
  '#000000 '
);
