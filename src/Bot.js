class Bot {
  constructor(name, avatar, actions, color) {
    this.name = name;
    this.avatar = avatar;
    this.actions = actions;
    this.color = color;
  }

  async respondTo(message) {
    const command = message.trim().toLowerCase();
    const action = this.actions.find((act) => act.command === command);
    if (action) {
      return this.performAction(action);
    }

    return null;
  }

  async performAction(action) {
    if (action.type === 'apiCall') {
      try {
        const data = await action.response();
        return data;
      } catch (error) {
        return 'Une erreur est survenue.';
      }
    } else if (action.type === 'text') {
      return action.response();
    }

    return 'Commande non reconnue.';
  }
}

export default Bot;
