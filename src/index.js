import Chat from './Chat';
import {
  lloyd, kai, zane, jay, cole
} from './BotsConfig';
import './index.scss';

const bots = [lloyd, kai, zane, jay, cole];

const test = new Chat(bots);
test.run();
